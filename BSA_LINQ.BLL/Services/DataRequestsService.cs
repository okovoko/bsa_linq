﻿using BSA_LINQ.DAL;
using BSA_LINQ.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA_LINQ.BLL.Services
{
    public class DataRequestsService
    {
        private DataProvider _data;

        public DataRequestsService()
        {
            this._data = DataProvider.Instance;
        }

        public Dictionary<Project, int> GetProjectUserTasksCount(int userId)
        {
            return (from t in this._data.Tasks
                    group t by t.ProjectId into tp
                    join p in this._data.Projects on tp.Key equals p.Id
                    where p.AuthorId == userId
                    select new
                    {
                        Project = p,
                        TasksNumber = tp.Count()
                    })
                   .ToDictionary(k => k.Project, v => v.TasksNumber);
        }

        public List<Task> GetUserTasks(int userId, int maxTaskNameLength = 45)
        {
            return this._data.Tasks
                        .Where(t => t.PerformerId == userId)
                        .Where(t => t.Name.Length < maxTaskNameLength)
                        .ToList<Task>();
        }

        public List<Tuple<int, string>> GetFinishedTasks(int userId, int finishYear = 2019)
        {
            return this._data.Tasks
                .Where(t => t.PerformerId == userId)
                .Where(t => t.FinishedAt.Year == 2019)
                .Select(t => Tuple.Create(t.Id, t.Name))
                .ToList<Tuple<int, string>>();
        }

        public List<Tuple<int, string, List<User>>> GetAgeLimitTeams(int ageLimitYears = 12)
        {
            return this._data.Users
                .Where(u => u.Birthday.Year > DateTime.Now.Year - ageLimitYears)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.TeamId)
                .Join(this._data.Teams, u => u.Key, t => t.Id, (u, t) =>
                Tuple.Create(t.Id, t.Name, u.ToList<User>()))
                .ToList<Tuple<int, string, List<User>>>();
        }

        public List<Tuple<User, List<Task>>> GetSortedUsers()
        {
            return this._data.Users
                .OrderBy(u => u.FirstName)
                .Select((u) => Tuple.Create(u, this._data.Tasks.Where(t => t.PerformerId == u.Id)
                            .OrderByDescending(t => t.Name.Length).ToList<Task>()))
                .ToList<Tuple<User, List<Task>>>();
        }

        public Tuple<User, Project, int, int, Task> GetUserLastProjectInfo(int userId)
        {
            return (from project in this._data.Projects
                    where project.AuthorId == userId
                    orderby project.CreatedAt descending
                    let user = this._data.Users.Where(x => x.Id == userId).FirstOrDefault()
                    let lastProjectTasksCount = this._data.Tasks.GroupBy(x => x.ProjectId)
                                                               .Where(x => x.Key == project.Id)
                                                               .FirstOrDefault().Count()
                    let unfinishedTasks = this._data.Tasks.Count(x => x.ProjectId == project.Id && x.StateId != (int)TaskStates.Finished)
                    let longestTask = this._data.Tasks.Where(x => x.PerformerId == user.Id)
                                          .OrderBy(x => x.CreatedAt)
                                          .ThenByDescending(x => x.FinishedAt).FirstOrDefault()
                    select Tuple.Create(user, project, lastProjectTasksCount, unfinishedTasks, longestTask)).FirstOrDefault();
        }

        public Tuple<Project, Task, Task, int> GetProjectShortInfo(int projectId)
        {
            return (from project in this._data.Projects
                    where project.Id == projectId
                    join task in this._data.Tasks on project.Id equals task.ProjectId
                    into tasksList
                    let longestDescriptionTask = (from task in this._data.Tasks
                                       where task.ProjectId == projectId
                                       orderby task.Description descending
                                       select task).FirstOrDefault()
                    let shortestNameTask = (from task in this._data.Tasks
                                        where task.ProjectId == projectId
                                        orderby task.Name
                                        select task).FirstOrDefault()
                    let users = from user in this._data.Users
                                where user.TeamId == project.TeamId && (project.Description.Length > 25 || tasksList.Count() < 3)
                                select user
                    select Tuple.Create(project, longestDescriptionTask, shortestNameTask, users.Count())).FirstOrDefault();
        }
    }
}
