﻿using BSA_LINQ.DAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_LINQ.DAL
{
    public class DataProvider
    {
        private static readonly Lazy<DataProvider> _instance = new Lazy<DataProvider>(() => new DataProvider());

        private System.Net.Http.HttpClient _client;
        private readonly string _baseURL;

        private IEnumerable<User> _users;
        IEnumerable<Project> _projects;
        IEnumerable<Team> _teams;
        IEnumerable<Task> _tasks;
        IEnumerable<TaskStateModel> _taskStates;

        DataProvider()
        {
            this._client = new System.Net.Http.HttpClient();
            this._baseURL = @"https://bsa2019.azurewebsites.net/";
        }
        ~DataProvider()
        {
            this._client.Dispose();
        }

        public IEnumerable<User> Users
        {
            get
            {
                if (this._users == null)
                {
                    this._users = JsonConvert.DeserializeObject<IEnumerable<User>>
                        (this._client.GetStringAsync(this._baseURL + @"api/users").Result);
                }
                return this._users;
            }
            private set { }
        }
        public IEnumerable<Task> Tasks
        {
            get
            {
                if (this._tasks == null)
                {
                    this._tasks = JsonConvert.DeserializeObject<IEnumerable<Task>>
                        (this._client.GetStringAsync(this._baseURL + @"api/tasks").Result);
                }
                return this._tasks;
            }
            private set { }
        }
        public IEnumerable<Project> Projects
        {
            get
            {
                if (this._projects == null)
                {
                    this._projects = JsonConvert.DeserializeObject<IEnumerable<Project>>
                        (this._client.GetStringAsync(this._baseURL + @"api/projects").Result);
                }
                return this._projects;
            }
            private set { }
        }
        public IEnumerable<Team> Teams
        {
            get
            {
                if (this._teams == null)
                {
                    this._teams = JsonConvert.DeserializeObject<IEnumerable<Team>>
                        (this._client.GetStringAsync(this._baseURL + @"api/teams").Result);
                }
                return this._teams;
            }
            private set { }
        }
        public IEnumerable<TaskStateModel> TaskStates
        {
            get
            {
                if (this._taskStates == null)
                {
                    this._taskStates = JsonConvert.DeserializeObject<IEnumerable<TaskStateModel>>
                        (this._client.GetStringAsync(this._baseURL + @"api/taskstates").Result);
                }
                return this._taskStates;
            }
            private set { }
        }

        public static DataProvider Instance { get { return _instance.Value; } }
    }
}
