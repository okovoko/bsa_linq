﻿using Newtonsoft.Json;
using System;

namespace BSA_LINQ.DAL.Models
{
    public class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("author_id")]
        public int AuthorId { get; set; }
        [JsonProperty("team_id")]
        public int TeamId { get; set; }
    }
}
