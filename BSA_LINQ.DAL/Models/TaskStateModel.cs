﻿using Newtonsoft.Json;

namespace BSA_LINQ.DAL.Models
{
    public class TaskStateModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
