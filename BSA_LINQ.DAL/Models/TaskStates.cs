﻿namespace BSA_LINQ.DAL.Models
{
    public enum TaskStates
    {
        Created = 0,
        Started,
        Finished,
        Cancelled
    }
}
