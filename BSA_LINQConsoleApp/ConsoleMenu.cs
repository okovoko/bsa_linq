﻿using BSA_LINQ.BLL.Services;
using BSA_LINQ.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_LINQConsoleApp
{
    class ConsoleMenu
    {
        private DataRequestsService _dataService;
        bool exit;

        public ConsoleMenu()
        {
            this._dataService = new DataRequestsService();
        }

        private void WriteLine(string text, ConsoleColor consoleColor = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public void Run()
        {
            while (!exit)
            {
                ShowMenuItems();
                int userInput = GetInput();
                Perform(userInput);
            }
        }

        private void Perform(int userInput)
        {
            switch (userInput)
            {
                case 1:
                    DisplayProjectUserTasksCount();
                    break;
                case 2:
                    DisplayUserTasks();
                    break;
                case 3:
                    DisplayFinishedTasks();
                    break;
                case 4:
                    DisplayAgeLimitTeams();
                    break;
                case 5:
                    DisplaySortedUsers();
                    break;
                case 6:
                    DisplayUserLastProjectInfo();
                    break;
                case 7:
                    DisplayProjectShortInfo();
                    break;
                case 8:
                    Exit();
                    break;

                default:
                    WriteLine("Please, Enter the number from 0 to 8.", ConsoleColor.Red);
                    break;
            }

            Console.WriteLine("Press enter");
            Console.ReadLine();
        }


        private int GetInput()
        {
            int userInput;
            do
            {
                WriteLine("Please, Enter your choice as a number:");
            } while (!int.TryParse(Console.ReadLine(), out userInput));

            return userInput;
        }

        private void ShowMenuItems()
        {
            Console.Clear();

            WriteLine("Choose option:");

            Console.WriteLine();

            WriteLine("1 - Display task count from the project of specific user");
            WriteLine("2 - Display tasks with description less than 45 symbols for the specific user");
            WriteLine("3 - Display finished tasks' id and name for the specific user (finishing year is 2019)");
            WriteLine("4 - Display teams collection with member older than 12 (Sorted by year of registration in descending order)");
            WriteLine("5 - Display User-Tasks (Sorted)");
            WriteLine("6 - Display User - Last user's project - Last project tasks' count - Unfinished tasks' count - The longest task");
            WriteLine("7 - Display Project - The longest project's task (by description) - The shortest project's task (by name) - Users' count ( Project description > 25 OR tasks' count < 3)");
            WriteLine("8 - Exit");

            Console.WriteLine();
        }

        private void DisplayProjectUserTasksCount()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = this._dataService.GetProjectUserTasksCount(input);
            foreach (var pair in data)
            {
                WriteLine($"Project id: {pair.Key.Id}\tProject name: {pair.Key.Name}\tTasks count: {pair.Value}");
            }
        }

        private void DisplayUserTasks()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = this._dataService.GetUserTasks(input);
            foreach (var task in data)
            {
                WriteLine($"Task id: {task.Id}\tTask name: {task.Name}\nTasks state: {(TaskStates)task.StateId}");
                WriteLine($"Project id: {task.ProjectId}");
                WriteLine($"Created at: {task.CreatedAt.ToString()}\tFinished at: {task.FinishedAt.ToString()}\n");
            }
        }

        private void DisplayFinishedTasks()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:\nFinishing year is 2019", ConsoleColor.Green);
            var data = this._dataService.GetFinishedTasks(input);
            foreach (var task in data)
            {
                WriteLine($"Task id: {task.Item1}\tTask name: {task.Item2}");
            }
        }

        private void DisplayAgeLimitTeams()
        {
            WriteLine("Answer:", ConsoleColor.Green);
            var data = this._dataService.GetAgeLimitTeams();
            foreach (var team in data)
            {
                WriteLine($"Team id: {team.Item1}\tTeam name: {team.Item2}");
                foreach (var user in team.Item3)
                {
                    WriteLine($"User id: {user.Id}\t User name: {user.FirstName} {user.LastName}\tRegistered at: {user.RegisteredAt}");
                }
                Console.WriteLine();
            }
        }

        private void DisplaySortedUsers()
        {
            WriteLine("Answer:", ConsoleColor.Green);
            var data = this._dataService.GetSortedUsers();
            foreach (var user in data)
            {
                WriteLine($"User id: {user.Item1.Id}\t User name: {user.Item1.FirstName} {user.Item1.LastName}");
                foreach (var task in user.Item2)
                {
                    WriteLine($"Task id: {task.Id}\tTask name: {task.Name}");
                }
                Console.WriteLine();
            }
        }

        private void DisplayUserLastProjectInfo()
        {
            WriteLine("Enter User id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = this._dataService.GetUserLastProjectInfo(input);
            WriteLine($"User id: {data.Item1.Id}\tUser name: {data.Item1.FirstName} {data.Item1.LastName}");
            WriteLine($"Last project id: {data.Item2.Id} Name: {data.Item2.Name}");
            WriteLine($"Tasks count: {data.Item3}\tUnfinished tasks coun: {data.Item4}");
            WriteLine($"The longest task: id: {data.Item5.Id} name: {data.Item5.Name}");
        }

        private void DisplayProjectShortInfo()
        {
            WriteLine("Enter Project id:");
            var input = GetInput();

            WriteLine("Answer:", ConsoleColor.Green);
            var data = this._dataService.GetProjectShortInfo(input);
            WriteLine($"Project id: {data.Item1.Id} Name: {data.Item1.Name}");
            WriteLine($"The longest project's task (by description):");
            if (data.Item2 == null)
            {
                WriteLine("There are no tasks", ConsoleColor.Red);
            }
            else
            {
                WriteLine($"Id: {data.Item2.Id} Name: {data.Item2.Name}");
            }
            WriteLine($"The shortest project's task (by name):");
            if (data.Item3 == null)
            {
                WriteLine("There are no tasks", ConsoleColor.Red);
            }
            else
            {
                WriteLine($"Id: {data.Item3.Id} Name: {data.Item3.Name}");
            }
            
            WriteLine($"Users' count ( Project description > 25 OR tasks' count < 3): {data.Item4}");
        }

        private void Exit()
        {
            exit = true;
        }
    }
}
